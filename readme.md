 

# Pushing Docker Image to Docker Hub

## 1. Create a Docker Hub Account

- If you don’t have a Docker Hub account, create one at [Docker Hub](https://hub.docker.com/).

## 2. Login to Docker Hub from the CLI

- Open your terminal/command prompt.
- Run the following command and enter your Docker Hub username and password when prompted:

    ```sh
    docker login
    ```

## 3. Create a Local Docker Image

- If you don’t have a Docker image yet, you need to create one by writing a Dockerfile.
- Here’s a simple example of a Dockerfile:

    ```Dockerfile
    FROM alpine:latest

    CMD ["echo", "Hello, World!"]
    ```

- Build the Docker image with the following command:

    ```sh
    docker build -t my-image .
    ```

## 4. Tag the Docker Image

- Tag the image with your username and repository name using the following syntax:

    ```sh
    docker tag local-image:tagname username/repository:tagname
    ```

- For example:

    ```sh
    docker tag my-image:latest username/my-image:latest
    ```

- Replace `username` with your Docker Hub username and `my-image` with the desired repository name.

## 5. Push the Docker Image to Docker Hub

- Push the tagged image to Docker Hub with the `docker push` command:

    ```sh
    docker push username/my-image:latest
    ```

- Replace `username` with your Docker Hub username and `my-image` with the repository name.

## 6. Verify the Push

- After the push is successful, verify that your image is now available on Docker Hub by visiting your repository:

    ```
    https://hub.docker.com/r/username/my-image
    ```

- Replace `username` and `my-image` with your Docker Hub username and repository name.

## 7. Set Repository to Public or Private

- By default, the repository on Docker Hub is public.
- If you want to make it private, navigate to the repository settings on Docker Hub and select “Private”.



# Pushing Docker Image to GitLab Container Registry

## 1. Create a GitLab Account/Project

- If you don’t have a GitLab account, create one at [GitLab](https://gitlab.com/users/sign_in).
- Once you have an account, create a new project where you will push your Docker image.

## 2. Install Docker

- If you haven’t installed Docker yet, download and install it from the [official Docker website](https://docs.docker.com/get-docker/).

## 3. Login to GitLab Container Registry

- Open your terminal/command prompt.
- Run the following command and enter your GitLab username and password (or access token) when prompted:

    ```sh
    docker login registry.gitlab.com
    ```

## 4. Create a Local Docker Image

- If you don’t have a Docker image yet, you need to create one by writing a Dockerfile.
- Here’s a simple example of a Dockerfile:

    ```Dockerfile
    FROM alpine:latest

    CMD ["echo", "Hello, World!"]
    ```

- Build the Docker image with the following command:

    ```sh
    docker build -t my-image .
    ```

## 5. Tag the Docker Image

- Tag the image with the registry URL, your username, and the project/repository name using the following syntax:

    ```sh
    docker tag local-image:tagname registry-url/username/repository:tagname
    ```

- For example:

    ```sh
    docker tag my-image:latest registry.gitlab.com/username/my-project/my-image:latest
    ```

- Replace `username` with your GitLab username, `my-project` with your GitLab project name, and `my-image` with the desired image name.

## 6. Push the Docker Image to GitLab Container Registry

- Push the tagged image to GitLab Container Registry with the `docker push` command:

    ```sh
    docker push registry.gitlab.com/username/my-project/my-image:latest
    ```

- Replace `username` with your GitLab username, `my-project` with your GitLab project name, and `my-image` with the image name.

## 7. Verify the Push

- After the push is successful, verify that your image is now available on the GitLab Container Registry by visiting the Container Registry section of your project on the GitLab website.
